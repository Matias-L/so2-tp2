#include <stdlib.h>
#include <stdio.h>
#include <netcdf.h>
#include <omp.h>

/* Handle errors by printing an error message and exiting with a
 * non-zero status. */
#define ERRCODE 2
#define ERR(e) {printf("Error: %s\n", nc_strerror(e)); exit(ERRCODE);}

/* nombre del archivo a leer */
#define FILE_NAME "../../Recursos/OR_ABI-L2-CMIPF-M6C02_G16_s20191011800206_e20191011809514_c20191011809591.nc"
#define ARCHIVO_SALIDA "./salida.nc"
/* Lectura de una matriz de 21696 x 21696 */
#define NX 21696
#define NY 21696

#define OUT_NX 20
#define OUT_NY 20
//float data_in[NX][NY];


int
main()
{
    int ncid, varid;
    int out_ncid, out_varid, out_xid, out_yid, out_dimids[1];
    //float data_in[NX][NY];
    //int x, y, retval;
    int retval;
    /*float **data_in;
    printf("Termine calloc...\n");
    data_in=calloc(NX*NY,sizeof(float*));
	*/
	float out_arregloTemporal[OUT_NY][OUT_NX];
    printf("Malloc entrada...\n");
    float **data_in=NULL;
    data_in=malloc(NY*sizeof(float*));

    for (int i = 0; i < NY; ++i)
    {
    	data_in[i]=malloc(NX*sizeof(float));
    }

    /*
		¿va malloc de salida?
    */

    printf("nc open...\n");
    if ((retval = nc_open(FILE_NAME, NC_NOWRITE, &ncid)))
        ERR(retval);
    
    printf("varId...\n");
    /* Obtenemos elvarID de la variable CMI. */
    if ((retval = nc_inq_varid(ncid, "CMI", &varid)))
        ERR(retval);

    printf("Leer matriz...\n");
    /* Leemos la matriz. */
    if ((retval = nc_get_var_float(ncid, varid, &data_in[0][0])))
        ERR(retval);
    
    /* el desarrollo acá */

    printf("Desarrollo...\n");

    /*Proceso de creacion de un NC:
    	1)nc_create
    	2)nc_def_dim
    	3)nc_def_var
    	4)nc_put_att

    	5)nc_enddef

    	6)nc_put_var

    	7)nc_close


    */




    printf("NC Salida...\n");
    //1 - nc_create(nombre salida, modo, referencia al ID)
    if((retval=nc_create(ARCHIVO_SALIDA, NC_CLOBBER, &out_ncid)))
    	ERR(retval)
    //2 - nc_defdim(ncid, char* nombreDimension, size_t len, int *dimidp)
    //Defino las dos dimenciones, x e y

    printf("DIMX\n");
    if((retval=nc_def_dim(out_ncid, "out_nx",sizeof(int), &out_xid)))
    	ERR(retval)
    printf("DIMY\n");
    if((retval=nc_def_dim(out_ncid, "out_ny",sizeof(int), &out_yid)))
    	ERR(retval)    

    //3 - nc_def_var(int ncid, char* name, nc_type xtype,
    //		int dims, int dimids[], int *varidp)
    printf("DEF VAR\n");
    out_dimids[0]=out_varid;
    if((retval=nc_def_var(out_ncid, "outCMI",NC_FLOAT,0, out_dimids,&out_varid)))
    	ERR(retval)    

    //4 - nc_put_att_float(int ncid, int varid, char* name, nc_type type
    // size_t len, float *fp)
    //Aca lo cambie por atributo tipo texto. Tengo entendido que el 
    //atributo es una caracteristica de la variable. Aca pongo
    //que su atributo es el tipo de variable (algo realmente redundante)
    printf("PUT ATT\n");
    if((retval=nc_put_att_text(out_ncid, out_varid, "ATRIBUTO",10,"tipo float")))
    	ERR(retval)    

    //5 - nc_enddef
    printf("ENDDEF\n");
    if((retval=nc_enddef(out_ncid)))
    	ERR(retval)    





    printf("Bucle...\n");
    for (int nFil = 0; nFil < 20; nFil++)
    {
    	for (int nCol = 0; nCol < 20; nCol++)
    	{
    		out_arregloTemporal[nFil][nCol]=data_in[nFil][nCol];
    		printf("%f ", out_arregloTemporal[nFil][nCol]);
    

    	}
    	printf("\n");
    }
//***************VER ESTOS DOS RENGLONES
    if((retval=nc_put_vara_float(out_ncid, out_varid, out_arregloTemporal[0][0])))
    	ERR(retval)


    /*Fin desarrollo*/
    printf("Cerrando1...\n");
    /* Se cierra el archivo y liberan los recursos*/
    if ((retval = nc_close(ncid)))
        ERR(retval);  
    printf("Cerrando2...\n");
    if ((retval = nc_close(out_ncid)))
        ERR(retval);  



    printf("Free...\n");
    free(data_in);
    return 0;
}
