#include <stdlib.h>
#include <stdio.h>
#include <netcdf.h>

/* Handle errors by printing an error message and exiting with a
 * non-zero status. */
#define ERRCODE 2
#define ERR(e)                                 \
    {                                          \
        printf("Error: %s\n", nc_strerror(e)); \
        exit(ERRCODE);                         \
    }

/* nombre del archivo a leer */

#define FILE_NAME_IN "OR_ABI-L2-CMIPF-M4C13_G16_s20161811455312_e20161811500135_c20161811500199.nc"
//#define FILE_NAME_IN "OR_ABI.nc"
#define FILE_NAME_OUT "OR_ABI_BFilter.nc"

/* Lectura de una matriz de 21696 x 21696 */
//#define NX 21696
#define NX 5424
//#define NY 21696
#define NY 5424
#define NDIMS 2

//float data_in[NX][NY];

int main()
{
    float **data_in;
    float **data_out;
    int ncid_i, varid_i;
    int retval_i;
    int dimids[NDIMS];
    int i; //Recorrer filas y columnas

    float w_matrix[3][3] = {
        {-1.0, -1.0, -1.0},
        {-1.0, 8.0, -1.0},
        {-1.0, -1.0, -1.0}};

    //int x, y;

    // Reserva de Memoria
    data_in = calloc(NX, sizeof *data_in);
    data_out = calloc(NX, sizeof *data_out);
    printf("ENTRO\n");
    for (i = 0; i < NY; i++)
    {
        data_in[i] = calloc(NY, sizeof *data_in[i]);
        data_out[i] = calloc(NY, sizeof *data_out[i]);
    }
    printf("SALGO\n");

    printf("ABRO\n");
    if ((retval_i = nc_open(FILE_NAME_IN, NC_NOWRITE, &ncid_i)))
        ERR(retval_i);

    printf("VAR_ID\n");
    /* Obtenemos elvarID de la variable CMI. */
    if ((retval_i = nc_inq_varid(ncid_i, "CMI", &varid_i)))
        ERR(retval_i);

    printf("LEO\n");
    /* Leemos la matriz. */
    if ((retval_i = nc_get_var_float(ncid_i, varid_i, &data_in[0][0])))
        ERR(retval_i);

    printf("LEO_2\n");

    /* Imprimo la matrix */
    // Dibujamos la Matriz en pantalla
    /*for (i = 0; i < NX; i++)
    {
        printf("\n");
        for (int j = 0; j < NY; j++)
        {
            printf("\t%lf", data_in[i][j]);
        }
    }*/

    /* el desarrollo acá */

    printf("DESARROLLO\n");
    float sumatoria;
    for (int y = 1; y < NY - 1; y++)
    {
        for (int x = 1; x < NX - 1; x++)
        {
            sumatoria = 0.0;
            for (int k = -1; k <= 1; k++)
            {
                for (int j = -1; j <= 1; j++)
                {
                    sumatoria = sumatoria + w_matrix[j + 1][k + 1] * data_in[y - j][x - k];
                }
            }
            data_out[y][x] = sumatoria;
            //printf("\t%lf", data_out[y][x]);
        }
    }

    /* Se cierra el archivo y liberan los recursos*/
    if ((retval_i = nc_close(ncid_i)))
        ERR(retval_i);

    printf("1\n");
    int ncid_o, x_dimid, y_dimid, varid_o, retval_o;
    //varid_o = varid_i;
    /* Create the file. The NC_NETCDF4 parameter tells netCDF to create
    * a file in netCDF-4/HDF5 standard. */
    printf("2\n");
    if ((retval_o = nc_create(FILE_NAME_OUT, NC_CLASSIC_MODEL, &ncid_o)))
        ERR(retval_o);
    /* Define the dimensions. */
    printf("3\n");
    if ((retval_o = nc_def_dim(ncid_o, "x", NX, &x_dimid)))
        ERR(retval_o);
    printf("4\n");
    if ((retval_o = nc_def_dim(ncid_o, "y", NY, &y_dimid)))
        ERR(retval_o);

    /* The dimids array is used to pass the IDs of the dimensions of
    * the variable. */
    dimids[0] = x_dimid;
    dimids[1] = y_dimid;

    /* Define the variable. The type of the variable in this case is
    * NC_INT (4-byte integer). */
    printf("5\n");
    if ((retval_o = nc_def_var(ncid_o, "CMI", NC_FLOAT, NDIMS,
                               dimids, &varid_o)))
        ERR(retval_o);

    printf("6\n");
    /* End define mode. This tells netCDF we are done defining
    * metadata. */
    if ((retval_o = nc_enddef(ncid_o)))
        ERR(retval_o);

    /* No need to explicitly end define mode for netCDF-4 files. Write
    * the pretend data to the file. */
    printf("7\n");
    if ((retval_o = nc_put_var_float(ncid_o, varid_o, &data_out[0][0])))
        ERR(retval_o);
    /* Close the file. */
    printf("8\n");
    if ((retval_o = nc_close(ncid_o)))
        ERR(retval_o);
    printf("*** SUCCESS writing example file BFilter.nc!\n");
    return 0;
}
