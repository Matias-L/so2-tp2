#include <stdlib.h>
#include <stdio.h>
#include <netcdf.h>
#include <omp.h>

/* Handle errors by printing an error message and exiting with a
 * non-zero status. */
#define ERRCODE 2
#define ERR(e) {printf("Error: %s\n", nc_strerror(e)); exit(ERRCODE);}

/* nombre del archivo a leer */
#define FILE_NAME "salida.nc"

/* Lectura de una matriz de 21696 x 21696 */
#define NX 20
#define NY 20
//float data_in[NX][NY];


int
main()
{
    int ncid, varid;
    //float data_in[NX][NY];
    //int x, y, retval;
    int retval;
    /*float **data_in;
    printf("Termine calloc...\n");
    data_in=calloc(NX*NY,sizeof(float*));
    */
    printf("Malloc...\n");
    float **data_in=NULL;
    data_in=malloc(NY*sizeof(float*));

    for (int i = 0; i < NY; ++i)
    {
        data_in[i]=malloc(NX*sizeof(float));
    }


    printf("nc open...\n");
    if ((retval = nc_open(FILE_NAME, NC_NOWRITE, &ncid)))
        ERR(retval);
    
    printf("varId...\n");
    /* Obtenemos elvarID de la variable CMI. */
    if ((retval = nc_inq_varid(ncid, "CMI", &varid)))
        ERR(retval);

    printf("Leer matriz...\n");
    /* Leemos la matriz. */
    if ((retval = nc_get_var_float(ncid, varid, &data_in[0][0])))
        ERR(retval);
    
    /* el desarrollo acá */

    printf("Desarrollo...\n");

    for (int nFil = 0; nFil < NY; nFil++)
    {
        for (int nCol = 0; nCol < NX; nCol++)
        {
            printf("%f ", data_in[nFil][nCol]);
        }
        printf("\n");
    }

    /*Fin desarrollo*/
    printf("Cerrando...\n");
    /* Se cierra el archivo y liberan los recursos*/
    if ((retval = nc_close(ncid)))
        ERR(retval);  

    printf("Free...\n");
    free(data_in);
    return 0;
}
