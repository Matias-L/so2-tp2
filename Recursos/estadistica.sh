#!/bin/bash

cores=2
until [ $cores -gt 65 ]
do
	for value in {1..30}
	do
		./TPOPENMP $(cores)
		grpof TPOPENMP gmon.out > $(cores)/profiling_$(value).txt
	done
cores= $(cores)*2
done


echo Listo