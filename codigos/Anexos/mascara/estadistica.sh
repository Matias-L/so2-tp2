#!/bin/bash
for cores in 1 2 4 8 
do
	for value in {1..30}
	do
		echo Ejecutando muestra $value con $cores hilos.
		./TPOPENMP $cores
	done
done

echo Listo
