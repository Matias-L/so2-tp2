#include <stdlib.h>
#include <stdio.h>
#include <netcdf.h>
#include <omp.h>
/* Handle errors by printing an error message and exiting with a
 * non-zero status. */
#define ERRCODE 2
#define ERR(e)                                 \
    {                                          \
        printf("Error: %s\n", nc_strerror(e)); \
        exit(ERRCODE);                         \
    }
#define FILE_NAME_IN "OR_ABI-L2-CMIPF-M6C02_G16_s20191011800206_e20191011809514_c20191011809591.nc" //Archivo entrada
#define FILE_NAME_OUT "ncSalida.nc"		//Archivo salida
//Dimensiones de la matriz de entrada
#define NX 21696
#define NY 21696
#define NDIMS 2	//Cantidad de dimenciones

void convolucion(float**, float**, int);

int main(int argc, char* argv[])
{
	int numHilos=atoi(argv[1]);
    float **data_in;
    float **data_out;
    int ncid_i, varid_i;
    int retval_i;
    int dimids[NDIMS];
    int i; //Recorrer filas y columnas
    int ncid_o, x_dimid, y_dimid, varid_o, retval_o; //Variables necesarias para el archivo de salida

    // Reserva de Memoria
    data_in = calloc(NX, sizeof *data_in);
    data_out = calloc(NX, sizeof *data_out);
    for (i = 0; i < NY; i++)
    {
        data_in[i] = calloc(NY, sizeof *data_in[i]);
        data_out[i] = calloc(NY, sizeof *data_out[i]);
    }
    //Apertura de archivo
    if ((retval_i = nc_open(FILE_NAME_IN, NC_NOWRITE, &ncid_i)))
        ERR(retval_i);
    // Obtenemos elvarID de la variable CMI. 
    if ((retval_i = nc_inq_varid(ncid_i, "CMI", &varid_i)))
        ERR(retval_i);
    // Leemos la matriz. 
    if ((retval_i = nc_get_var_float(ncid_i, varid_i, &data_in[0][0])))
        ERR(retval_i);
    //Procesa convolucion
    convolucion(data_in, data_out, numHilos);
    // Se cierra el archivo y liberan los recursos
    if ((retval_i = nc_close(ncid_i)))
        ERR(retval_i);
    //*****Creación del archivo de salida*********
    /* Create the file. The NC_NETCDF4 parameter tells netCDF to create
    * a file in netCDF-4/HDF5 standard. */
    if ((retval_o = nc_create(FILE_NAME_OUT, NC_CLASSIC_MODEL, &ncid_o)))
        ERR(retval_o);
    /* Define the dimensions. */
    if ((retval_o = nc_def_dim(ncid_o, "x", NX, &x_dimid)))
        ERR(retval_o);
    if ((retval_o = nc_def_dim(ncid_o, "y", NY, &y_dimid)))
        ERR(retval_o);
    /* The dimids array is used to pass the IDs of the dimensions of
    * the variable. */
    dimids[0] = x_dimid;
    dimids[1] = y_dimid;
    /* Define the variable. The type of the variable in this case is
    * NC_INT (4-byte integer). */
    if ((retval_o = nc_def_var(ncid_o, "CMI", NC_FLOAT, NDIMS, dimids, &varid_o))){
        ERR(retval_o);
    }
    /* End define mode. This tells netCDF we are done defining
    * metadata. */
    if ((retval_o = nc_enddef(ncid_o))){
        ERR(retval_o);
    }
    /* No need to explicitly end define mode for netCDF-4 files. Write
    * the pretend data to the file. */
    if ((retval_o = nc_put_var_float(ncid_o, varid_o, &data_out[0][0]))){
        ERR(retval_o);
    }
    /* Close the file. */
    if ((retval_o = nc_close(ncid_o))){
        ERR(retval_o);
    }
    printf("Todo listo\n");
    return 0;
}



void convolucion(float** data_in, float** data_out, int numHilos){

    float kernel[3][3] = {
        {-1.0, -1.0, -1.0},
        {-1.0, 8.0, -1.0},
        {-1.0, -1.0, -1.0}};
    float tInicio=0;
    omp_set_num_threads(numHilos);
	tInicio=omp_get_wtime();
	#pragma omp parallel for num_threads(numHilos)
 	        for (int y = 1; y < NY - 1; y++)
		    {
		     	for (int x = 1; x < NX - 1; x++)
		        {
		            data_out[y][x] = data_in[0][0]*kernel[2][2]+data_in[0][1]*kernel[2][1]+data_in[0][2]*kernel[2][0]+
		            				 data_in[1][0]*kernel[1][2]+data_in[1][1]*kernel[1][1]+data_in[1][2]*kernel[1][0]+
		            				 data_in[2][0]*kernel[0][2]+data_in[2][1]*kernel[0][1]+data_in[2][2]*kernel[0][0];
		        	
		        }
			}
	printf("Tiempo: %f\n", omp_get_wtime()-tInicio);
}

/*
Referencia del teorico

double ave=0.0, A[MAX]; int i;
#pragma omp parallel for reduction (+:ave)
for (i=0;i< MAX; i++) {
ave + = A[i];
}
ave = ave/MAX;


*/
