#!/bin/bash
for cores in 32 64 
do
mkdir /home/Estudiante54/$cores
	for value in {1..30}
	do
		echo Ejecutando muestra $value con $cores hilos.
		./V3 $cores
		gprof V3 gmon.out > $cores/profiling_$value.txt
	done
done

echo Listo
