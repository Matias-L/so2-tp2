#!/bin/bash
for cores in 1 2 4 8 16 32 64 
do
mkdir /home/Estudiante54/$cores
	for value in {16..20}
	do
		echo Ejecutando muestra $value con $cores hilos.
		./TPOPENMP $cores
		#gprof TPOPENMP gmon.out > $cores/profiling_$value.txt
		gprof gmon.out > $cores/profiling_$value.txt
	done
done

echo Listo
