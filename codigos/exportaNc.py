import matplotlib.pyplot as plt
from netCDF4 import Dataset
import math 
 
#  Imagen GOES .NC a visualizar
nc_origin = Dataset('/media/matias/MEGA SOPEDA/Carpeta personal/Fackiu/Sistemas operativos 2/2019/Practicos/nc/OR_ABI-L2-CMIPF-M6C02_G16_s20191011800206_e20191011809514_c20191011809591.nc', 'r')
nc_Salida = Dataset('/media/matias/MEGA SOPEDA/Carpeta personal/Fackiu/Sistemas operativos 2/2019/Practicos/nc/ncSalida.nc', 'r')

# Paso a samplear (no se muestran toodos los pixeles)
sample = 10
 
print 'Generando Imagen...' 
# Obtiene los pixeles de cada imagen
data_origin = nc_origin.variables['CMI'][::sample,::sample]
data_Salida = nc_Salida.variables['CMI'][::sample,::sample]
 
#Muestra y guarda NC entrada
plt.figure('Antes del Filtrado')
plt.imshow(data_origin,cmap = 'Greys')
plt.colorbar()
plt.savefig('NC_Before.png')
#Muestra y guarda NC Salida
plt.figure('Despues del Filtrado')
plt.imshow(data_Salida,cmap = 'Greys')
plt.colorbar()
plt.savefig('NC_After.png')
